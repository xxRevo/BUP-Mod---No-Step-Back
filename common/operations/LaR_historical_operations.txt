heavy_water_raid = { ### 1/50
	icon = GFX_historical_heavy_water
	map_icon = GFX_historical_heavy_water_small
	name = heavy_water_raid
	desc = heavy_water_raid_desc
	days = 60

	allowed = {
		NOT = { original_tag = GER }
	}
	available = { 
		has_war_with = GER
		network_strength = {
			target = GER
			state = 110
			value > 10
		}
		tag = ENG
	}
	visible = {
		FROM = {
			original_tag = GER
		}
		network_national_coverage = {
			target = GER
			value > 0
		}
		NOT = {
		    has_country_flag = GER_heavy_water
		}
	}

	operatives = 2

	# Integer [0-100] - Every operation has a hard requirement of network strength in the target country
	network_strength = 50

	equipment = {
		infantry_equipment = 100
		support_equipment = 100
	}

	on_start = {  # SCOPE_OPERATION
		
	}

	outcome_modifiers = { operation_outcome }
	cost_modifiers = { operation_cost }
	risk_modifiers = { operation_risk }

	outcome_potential = {
		custom_effect_tooltip = heavy_water_raid_outcome_tt
	}
	risk_chance = 0.50
	experience = 5
	# outcome is an effect that runs when the operation has completed
	outcome_execute = { # SCOPE_OPERATION
		FROM = { country_event = lar_heavy_water.1 }
		ROOT = { country_event = lar_heavy_water.2 }
	}
	
	# This is where all the separate phases are defined
	# Each phase for an operation may have a single actual phase assigned to it, which is determined here
	phases = { #infiltration 
		heavy_water_infiltration = { base = 100 }
	}
	phases = { 
		heavy_water_attack = { base = 100 }
	}
	phases = { 
		heavy_water_exfiltration = { base = 100 }
	}
}

operation_anthropoid = { ### 1/50
	icon = GFX_historical_capture_tito
	map_icon = GFX_historical_heavy_water_small
	name = operation_anthropoid
	desc = operation_anthropoid
	days = 15

	allowed = {
		NOT = { original_tag = GER }
	}
	available = { 
		has_war_with = GER
		network_strength = {
			target = GER
			state = 9
			value > 25
		}
		date > 1941.1.1
	}
	visible = {
		FROM = {
			original_tag = GER
		}
		network_national_coverage = {
			target = GER
			value > 0
		}
		GER = {
		    NOT = {
		    has_country_flag = GER_anthropoid
		    }
		}
	}

	operatives = 2

	# Integer [0-100] - Every operation has a hard requirement of network strength in the target country
	network_strength = 25

	equipment = {
		infantry_equipment = 500
		support_equipment = 100
	}

	on_start = {  # SCOPE_OPERATION
		
	}

	outcome_modifiers = { operation_outcome }
	cost_modifiers = { operation_cost }
	risk_modifiers = { operation_risk }

	outcome_potential = {
		custom_effect_tooltip = ant_tt
	}
	risk_chance = 0.50
	experience = 5
	# outcome is an effect that runs when the operation has completed
	outcome_execute = { # SCOPE_OPERATION
		9 = {
			damage_building = {
				type = infrastructure
				damage = 5
			}
			damage_building = {
			    type = arms_factory
				damage = 3
			}
		}
		GER = {
		    add_stability = -0.05
			add_war_support = -0.05
			set_country_flag = GER_anthropoid
		}
	}
	
	# This is where all the separate phases are defined
	# Each phase for an operation may have a single actual phase assigned to it, which is determined here
	phases = { #infiltration 
		ant_1 = { base = 100 }
	}
	phases = { 
		ant_2 = { base = 100 }
	}
	phases = { 
		ant_3 = { base = 100 }
	}
}

operation_himmler = { ### 1/50
	icon = GFX_historical_capture_tito
	map_icon = GFX_historical_heavy_water_small
	name = operation_himmler
	desc = operation_himmler
	days = 10

	allowed = {
		NOT = { original_tag = GER }
	}
	available = { 
		has_war_with = GER
		
		GER = {
		    has_country_flag = ger_himmler
		}
		
		
	}
	visible = {
		FROM = {
			original_tag = GER
		}
		network_national_coverage = {
			target = GER
			value > 0
		}
		GER = {
		    not = {has_country_flag = ger_himmler_assassinated} 
		}
	}

	operatives = 2

	# Integer [0-100] - Every operation has a hard requirement of network strength in the target country
	network_strength = 25

	equipment = {
		infantry_equipment = 1000
		support_equipment = 100
	}

	on_start = {  # SCOPE_OPERATION
		
	}

	outcome_modifiers = { operation_outcome }
	cost_modifiers = { operation_cost }
	risk_modifiers = { operation_risk }

	outcome_potential = {
		custom_effect_tooltip = himmler_tt
	}
	risk_chance = 0.50
	experience = 5
	# outcome is an effect that runs when the operation has completed
	outcome_execute = { # SCOPE_OPERATION
		GER = {
		    set_country_flag = ger_himmler_assassinated
		    
			create_country_leader = {
			name = "Hermann Göring"
			desc = "POLITICS_HERMANN_GORING_DESC"
			picture = "Portrait_Germany_Hermann_Goring.dds"
			expire = "1965.1.1"
			ideology = nazism
			traits = {
			}
		}
		}
		
		
	}
	
	# This is where all the separate phases are defined
	# Each phase for an operation may have a single actual phase assigned to it, which is determined here
	phases = { #infiltration 
		himm_1 = { base = 100 }
	}
	phases = { 
		himm_2 = { base = 100 }
	}
	phases = { 
		himm_3 = { base = 100 }
	}
}

bruneval_raid = { ### 1/50
	icon = GFX_operations_targeted_sabotage
	map_icon = GFX_historical_bruneval_raid_small
	name = bruneval_raid
	desc = bruneval_raid_desc
	days = 60

	allowed = {
		NOT = { original_tag = GER }
	}
	available = { 
		tag = FRA
		network_strength = {
			target = GER
			state = 16
			value > 10
		}
		has_war_with = GER
	}

	visible = {
		FRA = {
			has_war_with = GER
			has_capitulated = yes
		}
		FROM = {
			original_tag = GER
		}
		not = { has_global_flag = FRA_sabotaged_northern_france }
		network_national_coverage = {
			target = GER
			value > 0
		}
	}
	operatives = 2
	experience = 5

	# Integer [0-100] - Every operation has a hard requirement of network strength in the target country
	network_strength = 50

	equipment = {
		infantry_equipment = 100
		support_equipment = 100
	}

	on_start = {  # SCOPE_OPERATION
		
	}

	outcome_modifiers = { operation_outcome }
	cost_modifiers = { operation_cost }
	risk_modifiers = { operation_risk }

	outcome_potential = {
		custom_effect_tooltip = bruneval_raid_outcome_tt
	}
	risk_chance = 0.2
	# outcome is an effect that runs when the operation has completed
	outcome_execute = { # SCOPE_OPERATION
		16 = {
			damage_building = {
				type = infrastructure
				damage = 5
			}
			damage_building = {
			    type = arms_factory
				damage = 5
			}
		}
		15 = {
		    damage_building = {
			    type = infrastructure
				damage = 2
			}
			damage_building = {
			    type = industrial_complex
				damage = 2
			}
		}
		29 = {
		    damage_building = {
			    type = infrastructure
				damage = 3
			}
			damage_building = {
			    type = industrial_complex
				damage = 3
			}
		}
		FRA = {
		    set_country_flag = FRA_sabotaged_northern_france
		}
	}
	
	# This is where all the separate phases are defined
	# Each phase for an operation may have a single actual phase assigned to it, which is determined here
	phases = { #infiltration 
		bruneval_infiltration = { base = 100 }
	}
	phases = { 
		bruneval_attack = { base = 100 }
	}
	phases = { 
		bruneval_exfiltration = { base = 100 }
	}
}

organize_covert_raids_in_germany = { ### 1/50
	icon = GFX_operations_targeted_sabotage
	map_icon = GFX_historical_bruneval_raid_small
	name = germany_raid
	desc = bruneval_raid_desc
	days = 180

	allowed = {
		NOT = { original_tag = GER }
	}
	available = { 
	    num_of_operatives > 2
		has_war_with = GER
		FRA = {
		    has_capitulated = yes
		}
	}

	visible = {
		FROM = {
			original_tag = GER
		}
		network_national_coverage = {
			target = GER
			value > 0
		}
	}
	operatives = 2
	experience = 5

	# Integer [0-100] - Every operation has a hard requirement of network strength in the target country
	network_strength = 50

	equipment = {
		infantry_equipment = 250
		support_equipment = 50
	}

	on_start = {  # SCOPE_OPERATION
		
	}

	outcome_modifiers = { operation_outcome }
	cost_modifiers = { operation_cost }
	risk_modifiers = { operation_risk }

	outcome_potential = {
		custom_effect_tooltip = bruneval_raid_outcome_tt
	}
	risk_chance = 0.2
	# outcome is an effect that runs when the operation has completed
	outcome_execute = { # SCOPE_OPERATION
		random_list = {
		    10 = {
			    51 = {
				    damage_building = {
					    type = arms_factory
						damage = 1
					}
					damage_building = {
					    type = infrastructure
						damage = 1
					}
				}
				42 = {
				    damage_building = {
					    type = industrial_complex
						damage = 3
					}
					damage_building = {
					    type = infrastructure
						damage = 1
					}
				}
			}
			10 = {
			    7 = {
				    damage_building = {
					    type = arms_factory
						damage = 2
					}
					damage_building = {
					    type = industrial_complex
						damage = 2
					}
					damage_building = {
					    type = infantry_equipment
						damage = 1
					}
				}
			}
			10 = {
			    6 = {
				    damage_building = {
					    type = industrial_complex
						damage = 2
					}
					damage_building = {
					    type = arms_factory
						damage = 2
					}
					damage_building = {
					    type = industrial_complex
						damage = 1
					}
				}
			}
			10 = {
			    10 = {
				    damage_building = {
					    type = arms_factory
						damage = 4
					}
					damage_building = {
					    type = infrastructure
						damage = 1
					}
				}
			}
			10 = {
			    4 = {
				    damage_building = {
					    type = arms_factory
						damage = 1
					}
					damage_building = {
					    type = industrial_complex
						damage = 3
					}
					damage_building = {
					    type = infantry_equipment
						damage = 1
					}
				}
			}
			10 = {
			    9 = {
				    damage_building = {
					    type = arms_factory
						damage = 2
					}
					damage_building = {
					    type = industrial_complex
						damage = 2
					}
					damage_building = {
					    type = infrastructure
						damage = 1
					}
				}
			}
			10 = {
			    88 = {
				    damage_building = {
					    type = industrial_complex
						damage = 3
					}
					damage_building = {
					    type = arms_factory
						damage = 1
					}
					damage_building = {
					    type = infrastructure
						damage = 1
					}
					
				}
			}
			10 = {
			    37 = {
				    damage_building = {
					    type = industrial_complex
						damage = 3
					}
					damage_building = {
					    type = arms_factory
						damage = 1
					}
					damage_building = {
					    type = infrastructure
						damage = 1
					}
				}
			}
			10  ={
			    64 = {
				    damage_building = {
					    type = arms_factory
					    damage = 2
					}
					damage_building = {
					    type = industrial_complex
						damage = 2
					}
					damage_building = {
					    type = infrastructure
						damage = 1
					}
				}
			}
			10 = {
			    52 = {
				    damage_building = {
					    type = industrial_complex
						damage = 2
					}
					damage_building = {
					    type = arms_factory
						damage = 2
					}
					damage_building = {
					    type = infrastructure
						damage = 1
					}
				}
			}
		}
	}
	
	# This is where all the separate phases are defined
	# Each phase for an operation may have a single actual phase assigned to it, which is determined here
	phases = { #infiltration 
		bruneval_infiltration = { base = 100 }
	}
	phases = { 
		bruneval_attack = { base = 100 }
	}
	phases = { 
		bruneval_exfiltration = { base = 100 }
	}
}

preperations_for_dday_france = { ### 1/50
	icon = GFX_operations_targeted_sabotage
	map_icon = GFX_historical_bruneval_raid_small
	name = dday_raid
	desc = bruneval_raid_desc
	days = 7

	allowed = {
		NOT = { original_tag = GER }
	}
	available = { 
		has_war_with = GER
		date > 1942.1.1
		tag = USA
	}

	visible = {
		FROM = {
			original_tag = GER
		}
		not = { has_global_flag = GER_fra_sabotage }
		network_national_coverage = {
			target = GER
			value > 0
		}
		GER = {
		    not = { has_global_flag = GER_fra_sabotage }
		}
	}
	operatives = 2
	experience = 5

	# Integer [0-100] - Every operation has a hard requirement of network strength in the target country
	network_strength = 10

	equipment = {
		infantry_equipment = 500
		support_equipment = 50
	}

	on_start = {  # SCOPE_OPERATION
		
	}

	outcome_modifiers = { operation_outcome }
	cost_modifiers = { operation_cost }
	risk_modifiers = { operation_risk }

	outcome_potential = {
		custom_effect_tooltip = bruneval_raid_outcome_tt
	}
	risk_chance = 0.2
	# outcome is an effect that runs when the operation has completed
	outcome_execute = { # SCOPE_OPERATION
		16 = {
		    damage_building = {
			    type = air_base
				damage = 5
		    }
			damage_building = {
				type = infrastructure
				damage = 3
			}
		}
		18 = {
		    damage_building = {
			    type = air_base
				damage = 5
			}
			damage_building = {
			    type = infrastructure
				damage = 3
			}
		}
		17 = {
		    damage_building = {
			    type = air_base
				damage = 5
			}
			damage_building = {
			    type = infrastructure
				damage = 3
			}
		}
		28 = {
		    damage_building = {
			    type = air_base
				damage = 5
			}
			damage_building = {
			    type = infrastructure
				damage = 3
			}
		}
		GER = {
		    set_country_flag = GER_fra_sabotage
		}
	}
	
	# This is where all the separate phases are defined
	# Each phase for an operation may have a single actual phase assigned to it, which is determined here
	phases = { #infiltration 
		bruneval_infiltration = { base = 100 }
	}
	phases = { 
		bruneval_attack = { base = 100 }
	}
	phases = { 
		bruneval_exfiltration = { base = 100 }
	}
}