
state={
	id=41
	name="STATE_41" # Madrid
	manpower = 2269659

	state_category = city

	resources={
		aluminium=1 # was: 2
		steel=30
		chromium= 100
		tungsten=250 # was: 300
	}

	history={
		owner = SPR
		victory_points = {
			3938 50
		}
		victory_points = {
			858 1
		}
		buildings = {
			infrastructure = 3
			arms_factory = 2
			anti_air_building =10
			industrial_complex = 2
			
		}
		add_core_of = SPR
	}

	provinces={
		858 896 939 977 3794 3845 3938 3986 6914 9767 9800 11820 13224 
	}
}
